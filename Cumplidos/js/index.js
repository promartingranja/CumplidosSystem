let nombre = document.getElementById('nombre');
let boton = document.getElementById('boton');
let botonCerrar = document.getElementById("botonCerrar");
let fndMSJ = document.getElementById('msj');
let slider = document.getElementsByClassName('slider');
let idMsjNota = document.getElementById("idMsjNota");

let numCumplidos = cumplidos.length -1;
let vistos = [];

boton.addEventListener('click',function(){    
  let indice = 0;
  let numVistos = vistos.length -1;
  let invalido = false;
  if(numVistos == numCumplidos) vistos = [];

  do{
    invalido = false;
    indice = Math.floor(Math.random() * (numCumplidos + 1));  
    for (let c=0;c<=numVistos;c++){
      if(indice == vistos[c]){
        invalido = true;  
        break;
      }
    }    
  
  }while(invalido);
  vistos.push(indice);

  //alert(nombre.value +' ' + cumplidos[indice]);
  idMsjNota.innerHTML = nombre.value + " " + cumplidos[indice];
  nombre.value = "";
  nombre.focus();

  fndMSJ.style.visibility = "visible";
  slider[0].style.height = "700px";
  slider[0].style.width = "700px";  
  
});

botonCerrar.addEventListener('click', function(){
  fndMSJ.style.visibility = "collapse";
  slider[0].style.height = "500px";
  slider[0].style.width = "500px";  
});